console.log('Индекс JS загружается');

// import Swiper from 'swiper/bundle';
import isWebp from "./modules/mod.js";

// import controlSwiper from "../components/feedbackSlider/feedbackSlider.js";
import startGift from "../components/gift/gift.js";
import listenerButtonServices from "../components/services/services.js";
// import controlSwiper from "../components/feedbackSlider/feedbackSlider.js";
import questionsToggle from "../components/questions/questions.js";

isWebp();  

startGift();
listenerButtonServices();
// controlSwiper();
questionsToggle();

console.log('Индекс JS загрузился');