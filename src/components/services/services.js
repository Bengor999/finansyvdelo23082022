
export default function listenerButtonServices() {
    
    const buttonServices = document.querySelector('.services-content-header__button_js');
    
    buttonServices.addEventListener('click', clickButtonServices)

}
function clickButtonServices(e) {
    const contentServices = document.querySelector('.services-content-descripcion_js');
    // const blockVerticalButtonServices = document.querySelector('.block-vertical');
    e.target.classList.toggle('services-content-header__button_on_js')
    contentServices.classList.toggle('displayNone-js')
    // blockVerticalButtonServices.classList.toggle('displayNone-js')
}