  import Swiper from 'swiper/bundle';

export default function controlSwiper() {
  const swiperRight = new Swiper('.swiper-feedback_js', {
    direction: 'horizontal',
    loop: true,
    //расстояние между слайдами
    spaceBetween: 100,
    //центрируем слайд
    centeredSlides: true,
    //Индексный номер начального слайда
    // initialSlide: 1,
    nested: true,
    //количество слайдов на экране
    slidesPerView: 1,
    updateOnWindowResize: true,
    // autoHeight: true,
    autoplay: {
      delay: 1000,
      disableOnInteraction: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
  //вычисляем значение для слайдера в зависимости от ширины экрана
  // function slidesPerViewValue() {
    
  //   if (document.documentElement.clientWidth >= 1000) {
  //     return 1.8;
  //   } else {
  //     return 1.3;
  //   }
  // };
  

  // swiperLeft = new Swiper('.swiper-left', {
  //   spaceBetween: 0,
  //   centeredSlides: true,
  //   // initialSlide: 1,
  //   nested: true,
  //   direction: 'vertical',
  //   loop: false,
  //   slidesPerView: 5,
  //   updateOnWindowResize: true,
  //   autoHeight: true,
  // });
  // swiperRight.controller.control = swiperLeft;
  // swiperLeft.controller.control = swiperRight;
}