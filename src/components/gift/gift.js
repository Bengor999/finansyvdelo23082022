

export default function startGift() {
    ConversationGift();
}


function ConversationGift() {
    const phraseEl = document.querySelectorAll('.gift-phrase')
    const fon = document.querySelector('.gift-conversation__fon')

    phraseEl.forEach((function(item, index, array) {
        if (!item.classList.contains('displayNone-js')) {
            if(index == array.length - 1) {
                setTimeout(() => {
                    let it = item;
                    let a = array;

                    it.classList.toggle('displayNone-js')
                    a[0].classList.toggle('displayNone-js');
                    fon.classList.remove('rotateY180-js')
                }, 3500);
                setTimeout(ConversationGift, 4500);

            } else {
                setTimeout(() => {
                    let it = item;
                    let i = index;
                    let a = array;
                    
                    
                    it.classList.toggle('displayNone-js')
                    a[i+1].classList.toggle('displayNone-js');
                    // rotateFon()
                    fon.classList.toggle('rotateY180-js')

                    
                }, 1500);
                setTimeout(ConversationGift, 2500);

            }
        }

        // ... делать что-то с item
    }))
};

