

export default function questionsToggle () {
    const answerButtonOn = document.querySelectorAll('.questions-li__button_on_js');

    answerButtonOn.forEach(function(item) {
        item.addEventListener("click", visibleAnswer)
    });
}

function visibleAnswer(e) {
    e.target.parentNode.parentNode.classList.toggle('questions-li__wrapper_js')
    
    e.target.parentNode.nextElementSibling.classList.toggle('displayNone-js')

    e.target.classList.toggle('questions-li__button_on_js');
}
